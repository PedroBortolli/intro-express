import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 4
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const { body: { project: { slug } } } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  // código pt. 5
  test('DELETE /projects/:slug removes a matching project', async () => {
    const name = 'test123';
    await request(app).post('/projects').send({ name });
    const req = await request(app)
      .delete(`/projects/${name}`)
    expect(req.status).toBe(200);
  });

  // código pt. 6
  test('DELETE /projects/:slug/boards/:name removes the correct board from the project', async () => {
    const name = '123test'
    const project = { name };
    const { body: { project: { slug } } } = await request(app).post('/projects').send(project);

    const boardName = 'board123'
    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name: boardName });
    expect(body.board.name).toBe(boardName);

    const req = await request(app).delete(`/projects/${name}/boards/${boardName}`);
    expect(req.status).toBe(200);
  });

  // código pt. 7
  test('POST /projects/:slug/boards/:name/tasks adds a task to the project board', async () => {
    const name = 'bigproject'
    const project = { name };
    const { body: { project: { slug } } } = await request(app).post('/projects').send(project);

    const boardName = 'niceboard';
    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name: boardName });
    expect(body.board.name).toBe(boardName);

    const taskID = '127463';
    const req = await request(app)
      .post(`/projects/${name}/boards/${boardName}/tasks`)
      .send({ taskID });

    expect(req.status).toBe(200);
    expect(req.body.project.boards[0].tasks[0]).toBe(taskID);
  });

  // código pt. 8
  test('DELETE /projects/:slug/boards/name/tasks/:id deletes a task from the given project board', async () => {
    const name = 'finalproject'
    const project = { name };
    const { body: { project: { slug } } } = await request(app).post('/projects').send(project);

    const boardName = 'finalboard';
    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name: boardName });
    expect(body.board.name).toBe(boardName);

    const taskID = '999888777';
    let post = await request(app)
      .post(`/projects/${name}/boards/${boardName}/tasks`)
      .send({ taskID });
    expect(post.status).toBe(200);
    expect(post.body.project.boards[0].tasks[0]).toBe(taskID);

    let req = await request(app).delete(`/projects/${name}/boards/${boardName}/tasks/${taskID}`);
    expect(req.status).toBe(200);
  });
});
