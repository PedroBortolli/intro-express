const express = require('express');
const router = express.Router();

import { setupDB } from './db';

const findProject = async (req, res, next) => {
  const db = await setupDB();
  const { slug } = req.params;

  const project = await db.collection('projects').findOne({ slug });

  if (!project) {
    res.status(400).json({ error: `project ${slug} does not exist` });
  } else {
    req.body.project = project;
    next();
  }
};

const Singleton = (() => {
  let dbInstance;
  const connect = async () => {
    const db = await setupDB();
    return db;
  }
  return {
    getInstance: async () => {
      if (!dbInstance) dbInstance = await connect();
      return dbInstance;
    }
  }
})();

router.post('/', async (req, res) => {
  const db = await Singleton.getInstance();
  const { name } = req.body;
  const slug = name.toLowerCase();
  const project = { slug, boards: [] };
  const existingProject = await db.collection('projects').findOne({ slug });
  if (existingProject) {
    res.status(400).json({ error: `project ${slug} already exists` });
  } else {
    await db.collection('projects').insertOne(project);
    res.json({ project });
  }
});

router.get('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  res.json({ project });
});

router.post('/:slug/boards', findProject, async (req, res) => {
  const db = await Singleton.getInstance();
  const { name, project } = req.body;
  const board = { name, tasks: [] };
  project.boards.push(board);
  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);
  res.json({ board });
});

router.delete('/:slug', findProject, async (req, res) => {
  const db = await Singleton.getInstance();
  const { project } = req.body;
  console.log(project);
  await db
    .collection('projects')
    .deleteOne({ slug: project.slug })
  res.json({ project })
})

router.delete('/:slug/boards/:name', findProject, async (req, res) => {
  const db = await Singleton.getInstance();
  const { project } = req.body;
  const { name } = req.params;

  const newProjects = project.boards.reduce((tot, cur) => {
    if (cur.name !== name) tot.push(cur);
    return tot;
  }, []);

  if (newProjects.length === project.length)
    res.status(400).json({ error: "such board doesn't exist" });
  else {
    await db
      .collection('projects')
      .findOne({ slug: project.slug }, newProjects);
    res.json({ newProjects });
  }
})

router.post('/:slug/boards/:name/tasks', findProject, async (req, res) => {
  const db = await Singleton.getInstance();
  const { project, taskID } = req.body;
  const { name } = req.params;
  
  let added = false;
  project.boards.forEach(board => {
    if (board.name === name) {
      board.tasks.push(taskID)
      added = true;
    }
  });

  if (!added) res.status(400).json({ error: "such board doesn't exist "});
  else {
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);
    res.json({ project });
  }
})

router.delete('/:slug/boards/:name/tasks/:taskID', findProject, async (req, res) => {
  const db = await Singleton.getInstance();
  const { project } = req.body;
  const { name, taskID } = req.params;

  let removed = false;
  project.boards.forEach(board => {
    if (board.name === name) {
      const newTasks = board.tasks.reduce((tot, cur) => {
        if (cur !== taskID) tot.push(cur);
        else removed = true;
        return tot;
      }, []);
      board.tasks = newTasks;
    }
  });

  if (!removed) res.status(400).json({ error: "such task doesn't exist in that board" });
  else {
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);
    res.json({ project });
  }
})

module.exports = router;
